/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lkozloff
 */
public class HelloWorld {
    public static void main(String[] args){
        System.out.println("Hello World!");
        System.out.println("Now I will count to 100!");
        for(int i = 1; i<=100; i++){
            System.out.println(i);
        }
        
        System.out.println("And now I will count BACKWARDS");
         for(int i = 100; i>=0; i--){
            System.out.println(i);
        }
    }
}
